import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class FoodOrderMachine {
    private JPanel root;
    private JLabel topLabel;
    private JTabbedPane menuTab;
    private JPanel bowl;
    private JPanel topping;
    private JPanel soupSalad;
    private JPanel set;
    private JLabel orderedItemsLabel;
    private JLabel priceLabel;
    private JButton checkOut;
    private JButton beefBowl;
    private JButton porkBowl;
    private JButton chickenAndEggBowl;
    private JButton grilledBeefBowl;
    private JButton staminaBowl;
    private JButton grilledBeefRibBowl;
    private JTextPane orderItems;
    private JButton rawEgg;
    private JButton softBoiledEgg;
    private JButton greenOnionsAndRawEgg;
    private JButton cheese;
    private JButton kimchi;
    private JButton pickles;
    private JButton misoSoup;
    private JButton porkMisoSoup;
    private JButton shijimiClamSoup;
    private JButton salad;
    private JButton potatoSalad;
    private JButton saladSet;
    private JButton picklesSet;
    private JButton kimchiSet;


    /* セットメニューへ変更する */
    void changeToSet(List<String> list, int menu1, int menu2, String orderItem, int subPrice, int[] sumPrice){
        String orderItemsText = "";

        /* 変更するメニューだけ注文アイテムリストの表示形式を変える */
        list.set(menu1, "  - " + list.get(menu1));
        list.set(menu2, "  - " + list.get(menu2));
        for(int i=0; i<list.size(); i++){
                orderItemsText += list.get(i) + "\n";
        }
        orderItems.setText(orderItemsText);

        /* セットメニューへの変更を通知 */
        JOptionPane.showMessageDialog(
                null,
                "Change to set menu because the prices became cheaper.",
                "Message",
                JOptionPane.INFORMATION_MESSAGE
        );

        /* セットメニューへ変更する */
        list.set(menu1, "");
        list.set(menu2, "");
        list.add("->" + orderItem);
        sumPrice[0]-=subPrice;
        /* リストと合計金額を更新 */
        orderItemsText = "";
        for(int i=0; i<list.size(); i++){
            if(list.get(i)!="") {
                orderItemsText += list.get(i) + "\n";
            }
        }
        orderItems.setText(orderItemsText);
        priceLabel.setText("Total    "  + sumPrice[0] + "yen  " );
    }

    void order(String food, int price, int[] sumPrice){
        /* スタミナ丼以外の丼の場合はサイズの選択 */
        if(food.equals("Beef Bowl") || food.equals("Pork Bowl") || food.equals("Chicken And Egg Bowl") ||
                food.equals("Grilled Beef Rib Bowl") || food.equals("Grilled Beef Bowl")){
            String bowlSize[] = {"S", "M", "L"};
            int selection = JOptionPane.showOptionDialog(
                    null,
                    "Please choose size for " + food,
                    "Choose size",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    bowlSize,
                    bowlSize[0]
            );
            if (selection == 0 || selection == 1 ||selection == 2) {
                food += "(" + bowlSize[selection] + ")";
            }
            else{
                return;
            }
        }

        /* サラダの場合はドレッシングの選択 */
        if(food.equals("Salad") || food.equals("Potato Salad") || food.equals("Salad Set")){
            String dressing[] = {"Roasted Sesame Dressing", "Oil-Free Japanese Dressing"};
            int selection = JOptionPane.showOptionDialog(
                    null,
                    "Please choose dressing for " + food,
                    "Choose dressing",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    dressing,
                    dressing[0]
            );
            if (selection == 0 || selection == 1) {
                food += "(" + dressing[selection] + ")";
            }
            else{
                return;
            }
        }

        /* 注文確認ダイアログの表示 */
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + " ?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if(confirmation == 0){
            /* 注文完了の通知 */
            JOptionPane.showMessageDialog(
                    null,
                    "Thank you for ordering " + food + "! It will be served as soon as possible. ",
                    "Message",
                    JOptionPane.INFORMATION_MESSAGE
            );

            /* リストに商品と価格を追加 */
            String currentText = orderItems.getText();
            orderItems.setText(currentText + food + "    " + price + "yen\n");

            /* 合計価格ラベルの金額を更新 */
            sumPrice[0] += price;
            priceLabel.setText("Total    "  + sumPrice[0] + "yen  " );

            /* セットメニューが使えるときは自動で変更 */
            String[] orderItem = orderItems.getText().split("\n");
            List<String> orderItemList = new ArrayList<String>(Arrays.asList(orderItem));

            int[] cntSalad_rd={0,-1}, cntSalad_od={0,-1}, cntPickles={0,-1}, cntKimchi={0,-1}, cntMisoSoup={0,-1};
            for(int i=0; i<orderItemList.size(); i++) {
                /* セットメニューの単品を数える */
                if(orderItemList.get(i).split("    ")[0].equals("Salad(Roasted Sesame Dressing)")){
                    cntSalad_rd[0]++;
                    cntSalad_rd[1]=i;
                }
                if(orderItemList.get(i).split("    ")[0].equals("Salad(Oil-Free Japanese Dressing)")){
                    cntSalad_od[0]++;
                    cntSalad_od[1]=i;
                }
                if(orderItemList.get(i).split("    ")[0].equals("Pickles")){
                    cntPickles[0]++;
                    cntPickles[1]=i;
                }
                if(orderItemList.get(i).split("    ")[0].equals("Kimchi")){
                    cntKimchi[0]++;
                    cntKimchi[1]=i;
                }
                if(orderItemList.get(i).split("    ")[0].equals("Miso Soup")){
                    cntMisoSoup[0]++;
                    cntMisoSoup[1]=i;
                }

                /* セットメニューが使えたら変更 */
                if(cntMisoSoup[0]>=1 && cntSalad_rd[0]>=1){
                    changeToSet(orderItemList, cntSalad_rd[1], cntMisoSoup[1],
                            "Salad Set(Roasted Sesame Dressing)    194yen", 22, sumPrice);
                    break;
                }
                if(cntMisoSoup[0]>=1 && cntSalad_od[0]>=1){
                    changeToSet(orderItemList, cntSalad_od[1], cntMisoSoup[1],
                            "Salad Set(Oil-Free Japanese Dressing)    194yen", 22, sumPrice);
                    break;
                }
                if(cntMisoSoup[0]>=1 && cntPickles[0]>=1){
                    changeToSet(orderItemList, cntPickles[1], cntMisoSoup[1],
                            "Pickles Set    176yen", 20, sumPrice);
                    break;
                }
                if(cntMisoSoup[0]>=1 && cntKimchi[0]>=1){
                    changeToSet(orderItemList, cntKimchi[1], cntMisoSoup[1],
                            "Kimchi Set    167yen", 19, sumPrice);
                    break;
                }
            }
        }
    }

    public FoodOrderMachine() {
        /* 合計金額を保持するための変数 */
        int sumPrice[] = new int[1];
        sumPrice[0] = 0;

        beefBowl.setIcon(new ImageIcon(
                this.getClass().getResource("/FoodPhoto/beefBowl.jpg")
        ));
        porkBowl.setIcon(new ImageIcon(
                this.getClass().getResource("/FoodPhoto/porkBowl.jpg")
        ));
        chickenAndEggBowl.setIcon(new ImageIcon(
                this.getClass().getResource("/FoodPhoto/chickenAndEggBowl.jpg")
        ));
        grilledBeefRibBowl.setIcon(new ImageIcon(
                this.getClass().getResource("/FoodPhoto/grilledBeefRibBowl.jpg")
        ));
        grilledBeefBowl.setIcon(new ImageIcon(
                this.getClass().getResource("/FoodPhoto/grilledBeefBowl.jpg")
        ));
        staminaBowl.setIcon(new ImageIcon(
                this.getClass().getResource("/FoodPhoto/staminaBowl.jpg")
        ));
        rawEgg.setIcon(new ImageIcon(
                this.getClass().getResource("/FoodPhoto/rawEgg.jpg")
        ));
        softBoiledEgg.setIcon(new ImageIcon(
                this.getClass().getResource("/FoodPhoto/softBoiledEgg.jpg")
        ));
        greenOnionsAndRawEgg.setIcon(new ImageIcon(
                this.getClass().getResource("/FoodPhoto/greenOnionsAndRawEgg.jpg")
        ));
        cheese.setIcon(new ImageIcon(
                this.getClass().getResource("/FoodPhoto/cheese.jpg")
        ));
        kimchi.setIcon(new ImageIcon(
                this.getClass().getResource("/FoodPhoto/kimchi.jpg")
        ));
        pickles.setIcon(new ImageIcon(
                this.getClass().getResource("/FoodPhoto/pickles.jpg")
        ));
        misoSoup.setIcon(new ImageIcon(
                this.getClass().getResource("/FoodPhoto/misoSoup.jpg")
        ));
        porkMisoSoup.setIcon(new ImageIcon(
                this.getClass().getResource("/FoodPhoto/porkMisoSoup.jpg")
        ));
        shijimiClamSoup.setIcon(new ImageIcon(
                this.getClass().getResource("/FoodPhoto/shijimiClamSoup.jpg")
        ));
        salad.setIcon(new ImageIcon(
                this.getClass().getResource("/FoodPhoto/salad.jpg")
        ));
        potatoSalad.setIcon(new ImageIcon(
                this.getClass().getResource("/FoodPhoto/potatoSalad.jpg")
        ));
        saladSet.setIcon(new ImageIcon(
                this.getClass().getResource("/FoodPhoto/saladSet.jpg")
        ));
        picklesSet.setIcon(new ImageIcon(
                this.getClass().getResource("/FoodPhoto/picklesSet.jpg")
        ));
        kimchiSet.setIcon(new ImageIcon(
                this.getClass().getResource("/FoodPhoto/kimchiSet.jpg")
        ));

        beefBowl.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Beef Bowl", 448, sumPrice);
            }
        });
        porkBowl.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Pork Bowl", 415, sumPrice);
            }
        });
        chickenAndEggBowl.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Chicken And Egg Bowl", 586, sumPrice);
            }
        });
        grilledBeefRibBowl.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Grilled Beef Rib Bowl", 624, sumPrice);
            }
        });
        grilledBeefBowl.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Grilled Beef Bowl", 608, sumPrice);
            }
        });
        staminaBowl.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Stamina Bowl", 954, sumPrice);
            }
        });
        rawEgg.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Raw Egg", 78, sumPrice);
            }
        });
        softBoiledEgg.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Soft Boiled Egg", 88, sumPrice);
            }
        });
        greenOnionsAndRawEgg.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Green Onions And Raw Egg", 138, sumPrice);
            }
        });
        cheese.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Cheese", 108, sumPrice);
            }
        });
        kimchi.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Kimchi", 118, sumPrice);
            }
        });
        pickles.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Pickles", 128, sumPrice);
            }
        });
        misoSoup.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Miso Soup", 68, sumPrice);
            }
        });
        porkMisoSoup.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Pork Miso Soup", 198, sumPrice);
            }
        });
        shijimiClamSoup.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Shijimi Clam Soup", 178, sumPrice);
            }
        });
        salad.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Salad", 148, sumPrice);
            }
        });
        potatoSalad.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Potato Salad", 168, sumPrice);
            }
        });
        saladSet.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Salad Set", 194, sumPrice);
            }
        });
        picklesSet.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Pickles Set", 176, sumPrice);
            }
        });
        kimchiSet.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Kimchi Set", 167, sumPrice);
            }
        });

        priceLabel.setText("Total    "  + sumPrice[0] + "yen  " );

        checkOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if (confirmation == 0) {
                    /* 感謝メッセージの通知 */
                    JOptionPane.showMessageDialog(
                            null,
                            "Thank you. The total price is " + sumPrice[0] + " yen.",
                            "Message",
                            JOptionPane.INFORMATION_MESSAGE
                    );
                    /* 注文リストのクリア */
                    orderItems.setText("");
                    /* 合計価格ラベルの金額をクリア */
                    sumPrice[0] = 0;
                    priceLabel.setText("Total    "  + sumPrice[0] + "yen  " );
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachine");
        frame.setContentPane(new FoodOrderMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
